#
# variables used here are set in related docker-compose.yml file
#
# --------------------------------------------------------------------------- #
# first container (temporary one to use Go-lang)                              #
# --------------------------------------------------------------------------- #

ARG temporary_image
ARG base_image

# use a temporary image for compilation
FROM ${temporary_image} as golang-container

ARG maintainer
ARG package_list
ARG egem_repo
ARG egem_branch
ARG egem_dir

# set maintainer of this Dockerfile
LABEL maintainer="${maintainer}"

# install dependencies
RUN apk add --update --no-cache ${package_list}

# clone Go-EGEM from official repo
RUN git clone ${egem_branch} ${egem_repo} ${egem_dir}

# modify permissions for Go-EGEM directory
RUN chmod 755 -R ${egem_dir}

# run make command for Go-EGEM
RUN make -C ${egem_dir} egem

# --------------------------------------------------------------------------- #
# second container (main one to store Go-EGEM)                                #
# --------------------------------------------------------------------------- #

# use a base image for new container
FROM ${base_image}

ARG data_dir
ARG egem_dir

# create a mount point at Go-EGEM data directory
VOLUME ["${data_dir}"]

# install SSL root certificates
RUN apk add --update --no-cache ca-certificates

# copy Go-EGEM executable from previous Go-lang container
COPY --from=golang-container ${egem_dir}/build/bin/egem /usr/local/bin/

EXPOSE 8895/tcp
EXPOSE 8895/udp
EXPOSE 30666/tcp
EXPOSE 30666/udp
