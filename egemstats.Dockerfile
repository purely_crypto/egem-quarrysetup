#
# variables used here are set in related docker-compose.yml file
#
# --------------------------------------------------------------------------- #
# first container (temporary one to use Go-lang)                              #
# --------------------------------------------------------------------------- #

ARG temporary_image
ARG base_image

# use a temporary image for compilation
FROM ${temporary_image} as golang-container

ARG maintainer
ARG package_list
ARG egem_repo
ARG egem_branch
ARG egem_dir

# set maintainer of this Dockerfile
LABEL maintainer="${maintainer}"

# install dependencies
RUN apk add --update --no-cache ${package_list}

# clone Go-EGEM from official repo
RUN git clone --single-branch -b ${egem_branch} ${egem_repo} ${egem_dir}

# modify permissions for Go-EGEM directory
RUN chmod 777 -R ${egem_dir}

# run make command for NodeStats
RUN make -C ${egem_dir} stats

# --------------------------------------------------------------------------- #
# second container (main one to store NodeStats)                              #
# --------------------------------------------------------------------------- #

# use a base image for new container
FROM ${base_image}

ARG egem_dir

# copy NodeStats executable from previous Go-lang container
COPY --from=golang-container ${egem_dir}/build/bin/stats /usr/local/bin/

EXPOSE 8897/tcp
EXPOSE 8897/udp

