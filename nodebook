#!/bin/bash
#
# Script Variables
# Shared variables for EGEM Quarry Node scripts
#

# --------------------------------------------------------------------------

# golang version to use
export golang_version="1.13.8"

# link for golang download repository
export golang_repo="https://dl.google.com/go/"

# golang install directory
export golang_dir="/usr/local/go"

# golang install directory
export GOROOT="${golang_dir}"

# path for golang works
export GOPATH="${HOME}/go-workspace"

# add golang path to PATH variable, if it is not there
{ echo $PATH | grep ${GOROOT}/bin; } &>/dev/null || export PATH="${PATH}:${GOROOT}/bin"
{ echo $PATH | grep ${GOPATH}/bin; } &>/dev/null || export PATH="${PATH}:${GOPATH}/bin"

# --------------------------------------------------------------------------

# name of variables file
export nodebook="nodebook"

# name of update script
export nodeupdater="nodeupdater"

# name of node management script
export nodemanager="nodemanager"

# name of maintenance script that runs before setup
export presetup="presetup"

# name of maintenance script that runs after setup
export postsetup="postsetup"

# file to record events/errors
export egemlog="${HOME}/egem.log"

# file to save system information
export sysinfo="${HOME}/sysinfo"

# systemd service file for Go-EGEM
export egem_service="egem.service"

# systemd service file for nodestats
export nodestats_service="nodestats.service"

# systemd service file for nodeupdater
export nodeupdater_service="nodeupdater.service"

# Go-EGEM folder
export dir_go_egem="${HOME}/go-egem"

# live network data folder
export dir_live_net="${HOME}/live-net"

# repository name of Quarry Node scripts
export script_repo_name="egem-quarrysetup"

# directory of Quarry Node scripts
export dir_scripts="${HOME}/${script_repo_name}"

# --------------------------------------------------------------------------

# link for Quarry Node scripts repo
export script_repo_link="https://gitlab.com/ethergem/${script_repo_name}.git"

# link for Go-EGEM repo
export egem_git="https://gitlab.com/ethergem/go-egem.git"

# source branch of Go-EGEM repo
export goegem_branch="master"

# api link for script version check
export script_version_api="https://api.egem.io/api/v1/auto_updates?script-version=true"

# api link for goegem version check
export goegem_version_api="https://api.egem.io/api/v1/auto_updates?egem-version=true"

# link for config.toml file
export config_link="https://gitlab.com/purely_crypto/egem-configs/raw/master/config-qn.toml"

# file to check for upgrade permission
export upgrade_file_link="https://gitlab.com/purely_crypto/egem-quarrysetup/raw/master/upgrade_to_docker"

# new script
export new_script="https://gitlab.com/purely_crypto/egem-quarrysetup/raw/master/setup"

# action file link
export action_link="https://gitlab.com/purely_crypto/egem-quarrysetup/raw/master/action"

# --------------------------------------------------------------------------

# minimum RAM requirement in MB
export min_ram="450"

# ideal amount of RAM
export ideal_ram="1900"

# url for external ip check
export ipcheck_url="ipinfo.io/ip"

# path for swap file
export swapfile="/swapfile"
