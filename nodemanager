#!/bin/bash
#
# Node Manager
# Management script for EGEM Quarry Nodes
#

collect_info(){
    we_have_all_we_need="true"

    # check if file exists,
    if [ -f ${sysinfo} ]; then
        source ${sysinfo} &>/dev/null

        # check each variable, collect system info again if there are any empty variables
        for x in ${os_name_long:-empty} ${os_name_short:-empty} ${cpu_arch:-empty} ${total_ram:-empty} ${total_disk:-empty}
        do
            if [ "${x}" == "empty" ]; then
                we_have_all_we_need="false"
                break
            fi
        done
    else
        we_have_all_we_need="false"
    fi

    case ${we_have_all_we_need} in
    "false")
        rm -rf ${sysinfo} &>/dev/null

        subheader "collecting system info..."

        # -------------------------------------------------------

        subtext "Operating System"

            os_name_long=`lsb_release -d | tr -s '[:blank:]' ' ' | awk -F ':' '{$1="";print}' | xargs`

            echo 'export os_name_long="'${os_name_long}'"' >> ${sysinfo}

            if [ ! -z "$(echo ${os_name_long} | grep -i 'debian.*8')" ]; then
                os_name_short="Debian_8"
            elif [ ! -z "$(echo ${os_name_long} | grep -i 'debian.*9')" ]; then
                os_name_short="Debian_9"
            elif [ ! -z "$(echo ${os_name_long} | grep -i 'debian.*10')" ]; then
                os_name_short="Debian_10"
            elif [ ! -z "$(echo ${os_name_long} | grep -i 'raspbian.*9')" ]; then
                os_name_short="Raspbian_9"
            elif [ ! -z "$(echo ${os_name_long} | grep -i 'raspbian.*10')" ]; then
                os_name_short="Raspbian_10"
            elif [ ! -z "$(echo ${os_name_long} | grep -i 'centos.*7')" ]; then
                os_name_short="CentOS_7"
            elif [ ! -z "$(echo ${os_name_long} | grep -i 'ubuntu.*16')" ]; then
                os_name_short="Ubuntu_16"
            elif [ ! -z "$(echo ${os_name_long} | grep -i 'ubuntu.*18')" ]; then
                os_name_short="Ubuntu_18"
            elif [ ! -z "$(echo ${os_name_long} | grep -i 'ubuntu.*19')" ]; then
                os_name_short="Ubuntu_19"
            else
                os_name_short="unidentified"
            fi

            echo 'export os_name_short="'${os_name_short}'"' >> ${sysinfo}

        # -------------------------------------------------------

        subtext "CPU"

            case `uname -m` in
            "armv6l"|"armv7l")
                cpu_arch="armv6l"
            ;;
            "aarch64")
                cpu_arch="arm64"
            ;;
            "x86"|"i386"|"i686"|"32bit"|"32 bit")
                cpu_arch="386"
            ;;
            "x86_64"|"64bit"|"64 bit")
                cpu_arch="amd64"
            ;;
            *)
                error "detect CPU architecture (current one is not supported)"
            ;;
            esac

            echo 'export cpu_arch="'${cpu_arch}'"' >> ${sysinfo}

        # -------------------------------------------------------

        subtext "RAM"

            total_ram=`free -m | grep -i "mem:" | awk '{print $2}'`
            echo 'export total_ram="'${total_ram}'"' >> ${sysinfo}

        # -------------------------------------------------------
        #
        # subtext "Disk"
        #
        #     total_disk=`df --output=target,size,avail -hl | grep '/' -w | awk '{print $2}'`
        #     echo 'export total_disk="'${total_disk}'"' >> ${sysinfo}
    ;;
    esac

    source ${sysinfo} &>/dev/null
}

check_dependencies(){
    early_header "Preparation Steps"

        subheader "updating package database (might take a while)..."

            case ${package_manager} in
            "yum")
                run_pkg_manager "update"
                run_pkg_manager "upgrade"
            ;;
            *)
                run_pkg_manager "update"
                run_pkg_manager "upgrade"
            ;;
            esac

        subheader "removing golang (to install a custom version)..."

            package_list="golang"

            for pkg2remove in ${package_list}
            do
                run_pkg_manager "remove ${pkg2remove}"
            done

            run_pkg_manager "autoremove"

        subheader "adding necessary repositories..."

            case ${package_manager} in
            "yum")
                subtext "official Fedora EPEL repository"

                pkg "install epel-release"
            ;;
            *)
                subtext "official repositories (main, universe, restricted, multiverse)"

                pkg "install software-properties-common"

                repository_list="main universe restricted multiverse"

                for r in ${repository_list}
                do
                    add-apt-repository -y ${r} &>/dev/null
                done
            ;;
            esac

        subheader "checking if must-have packages are installed..."

            if hash wget &>/dev/null; then
                subtext ${GREEN}"wget${CYAN} is already installed."
            else
                subtext ${YELLOW}"wget${CYAN} is missing, installing now..."

                pkg "install wget"
            fi

            if hash lsb_release &>/dev/null; then
                subtext ${GREEN}"lsb-release${CYAN} is already installed."
            else
                subtext ${YELLOW}"lsb-release${CYAN} is missing, installing now..."

                case ${package_manager} in
                "yum")
                    pkg "install redhat-lsb" || pkg "install redhat-lsb-core"
                ;;
                *)
                    pkg "install lsb-release"
                ;;
                esac
            fi

            if hash git &>/dev/null; then
                subtext ${GREEN}"git${CYAN} is already installed."
            else
                subtext ${YELLOW}"git${CYAN} is missing, installing now..."

                pkg "install git"
            fi
}

set_env(){
    case "${1}" in
    "force")
        rm -rf ${HOME}/egem_variables &>/dev/null
    ;;
    esac

    if [ -f ${HOME}/egem_variables ]; then
        source ${HOME}/egem_variables &>/dev/null

        # a random variable to check if previous step was succesful
        [[ -z "${egem_service}" ]] && download_scripts
    else
        download_scripts
    fi
}

download_scripts(){
    script_branch="${script_branch:-master}"
    nodebook_link="https://gitlab.com/purely_crypto/egem-quarrysetup/raw/${script_branch}/nodebook"

    # import variables from official repo
    source <(wget -q --no-check-certificate -O - ${nodebook_link}) &>/dev/null || source <(wget -q --no-check-certificate -O - ${nodebook_link}) &>/dev/null

    temp_dir="${dir_scripts}_temp"
    rm -rf ${temp_dir} &>/dev/null

    { git clone --single-branch -b ${script_branch} ${script_repo_link} ${temp_dir} &>/dev/null || git clone --single-branch -b ${script_branch} ${script_repo_link} ${temp_dir} &>/dev/null; } && {
        chmod -R 777 ${temp_dir} &>/dev/null
        rm -rf ${dir_scripts} &>/dev/null && mv ${temp_dir} ${dir_scripts}

        chmod +x ${dir_scripts}/${nodemanager} ${dir_scripts}/${nodeupdater}

        ln -sf ${dir_scripts}/${nodemanager} /usr/bin/${nodemanager} &>/dev/null
        ln -sf ${dir_scripts}/${nodeupdater} /usr/bin/${nodeupdater} &>/dev/null
        ln -sf ${dir_scripts}/${nodebook} ${HOME}/egem_variables &>/dev/null

        # import variables from local variable list
        source ${HOME}/egem_variables &>/dev/null || error "read variable list"
        }
}

start_services(){
    services_to_start="${1}"

    systemctl daemon-reload &>/dev/null

    if [ -z "${services_to_start}" ]; then
        services_to_start="${egem_service} ${nodestats_service} ${nodeupdater_service}"
    fi

    echo

    for srv in ${services_to_start}
    do
        if [ -f /etc/systemd/system/${srv} ]; then
            echo ${GREEN}"Starting -> ${CYAN}${srv}"${NORMAL}

            systemctl enable ${srv} &>/dev/null
            systemctl start ${srv} &>/dev/null
        fi
    done

    echo
}

stop_services(){
    services_to_stop="${1}"

    systemctl daemon-reload &>/dev/null

    if [ -z "${services_to_stop}" ]; then
        services_to_stop="${egem_service} ${nodestats_service} ${nodeupdater_service}"
    fi

    for srv in ${services_to_stop}
    do
        if [ -f /etc/systemd/system/${srv} ]; then
            echo ${YELLOW}"Stopping -> ${CYAN}${srv}"${NORMAL}

            systemctl stop ${srv} &>/dev/null
        fi
    done

    if hash kill &>/dev/null; then
        killthis ${services_to_stop} &>/dev/null
    fi
}

restart_services(){
    restartwhat="${1}"

    stop_services "${restartwhat}"
    start_services "${restartwhat}"
}

killthis(){
    for pname in ${1}
    do
        ps -ef | grep ${pname} | grep -v $$ | grep -v grep  | awk '{print $2}' | xargs kill &>/dev/null
    done

    sleep 3s

    for pname in ${1}
    do
        ps -ef | grep ${pname} | grep -v $$ | grep -v grep  | awk '{print $2}' | xargs kill -9 &>/dev/null
    done
}

create_swapfile(){
    header "Calculating needed RAM/Swap size..."

    # detect swap size
    current_swap=`swapon -se | grep -vi 'size' | awk '{s+=$3}END{print s}'`

    if [ -z "${current_swap}" ]; then
        current_swap="0"
    fi

    if (("${current_swap}" > 0)); then
        current_swap=$(( ${current_swap} / 1024 ))
    fi

    # display RAM/swap info
    subheader "total RAM size  : ${YELLOW}${total_ram} MB"
    subheader "total SWAP size : ${YELLOW}${current_swap} MB"

    # calculate necessary swap size
    missing_swap="$((${ideal_ram}-(${total_ram}+${current_swap})))"

    if (("${missing_swap}" < 100)); then
        missing_swap="0"
    fi

    if (("${missing_swap}" > 0)); then
        subheader "creating a ${YELLOW}${missing_swap} MB${MAGENTA} swap file..."

        swapoff ${swapfile} &>/dev/null
        rm -rf ${swapfile} &>/dev/null

        dd if=/dev/zero of=${swapfile} bs=1MiB count=${missing_swap} &>/dev/null || error "create swap file (dd command)"

        chmod 600 ${swapfile} &>/dev/null
        mkswap ${swapfile} &>/dev/null || error "create swap file (mkswap)"

        subheader "activating new swap file..."

        # swapon ${swapfile} &>/dev/null || error "activate swap file (swapon)"
        swapon ${swapfile} &>/dev/null

        # if script couldn't activate swap file, check "total ram + total swap" size
        # continue if it is greater or equal than 900 MB, halt if it is less than 900 MB
        if [ $? != "0" ]; then
            if (("$((${total_ram}+${current_swap}))" < 900)); then
                error "activate swap file (swapon)"
            fi
        fi

        # sed -i -e "s:.*${swapfile}.*::g" -e ":^$:d" /etc/fstab
        echo "$(grep -v ${swapfile} /etc/fstab)" > /etc/fstab
        echo "${swapfile} none swap sw 0 0" >> /etc/fstab

        swapon -a &>/dev/null
    else
        subheader "skipping swap file creation (you have enough RAM/Swap)..."

        return 0
    fi
}

install_necessary_packages(){
    header "Installing necessary packages..."

    # workaround for a fail2ban error
    if [ ! -f /var/log/auth.log ]; then
        touch /var/log/auth.log &>/dev/null
        chmod 640 /var/log/auth.log &>/dev/null
    fi

    case ${package_manager} in
    "yum")
        package_list="autoconf automake binutils bison flex gcc gcc-c++ gettext libtool make patch pkgconfig redhat-rpm-config rpm-build rpm-sign"
    ;;
    *)
        package_list="build-essential"
    ;;
    esac

    case ${package_manager} in
    "yum")
        package_list="${package_list} firewalld"
    ;;
    *)
        package_list="${package_list} ufw"
    ;;
    esac

    package_list="${package_list} fail2ban nano ntpdate"

    for pkg2install in ${package_list}
    do
        pkg "install ${pkg2install}"
    done

    header "Synchronizing time..."

    ntpdate -s time.nist.gov
}

install_golang(){
    header "Installing Go-lang v${golang_version}..."

    # delete manually installed golang directory (leftover from previous setup)
    rm -rf ${golang_dir} &>/dev/null

    operating_system_type=`uname -s | tr -s '[:upper:]' '[:lower:]'`

    golang_file="go${golang_version}.${operating_system_type}-${cpu_arch}.tar.gz"

    golang_link="${golang_repo}${golang_file}"

    wget_parameters="-q --read-timeout 30 --waitretry 10 --tries 15 -c --retry-connrefused --no-dns-cache --no-check-certificate"

    wget ${wget_parameters} ${golang_link} -O ${golang_file} &>/dev/null || wget ${wget_parameters} ${golang_link} -O ${golang_file} &>/dev/null || error "download golang (${golang_file})"
    tar -C /usr/local -xzf ${golang_file} &>/dev/null || error "extract ${golang_file}"
    rm -rf ${golang_file} &>/dev/null

    if [ -f "${HOME}/.bashrc" ]; then
        echo "$(grep -v GOROOT ${HOME}/.bashrc | grep -v GOPATH)" > ${HOME}/.bashrc
    fi

    if [ -f "${HOME}/.profile" ]; then
        echo "$(grep -v GOROOT ${HOME}/.profile | grep -v GOPATH)" > ${HOME}/.profile
    fi

    if [ -f "/etc/profile" ]; then
        echo "$(grep -v GOROOT /etc/profile | grep -v GOPATH)" > /etc/profile
    fi

    echo "export GOROOT=${GOROOT}" >> ${HOME}/.bashrc
    echo "export GOPATH=${GOPATH}" >> ${HOME}/.bashrc
    echo '{ echo ${PATH} | grep ${GOROOT}/bin; } &>/dev/null || export PATH="${PATH}:${GOROOT}/bin"' >> ${HOME}/.bashrc
    echo '{ echo ${PATH} | grep ${GOPATH}/bin; } &>/dev/null || export PATH="${PATH}:${GOPATH}/bin"' >> ${HOME}/.bashrc

    echo "export GOROOT=${GOROOT}" >> ${HOME}/.profile
    echo "export GOPATH=${GOPATH}" >> ${HOME}/.profile
    echo '{ echo ${PATH} | grep ${GOROOT}/bin; } &>/dev/null || export PATH="${PATH}:${GOROOT}/bin"' >> ${HOME}/.profile
    echo '{ echo ${PATH} | grep ${GOPATH}/bin; } &>/dev/null || export PATH="${PATH}:${GOPATH}/bin"' >> ${HOME}/.profile

    echo "export GOROOT=${GOROOT}" >> /etc/profile
    echo "export GOPATH=${GOPATH}" >> /etc/profile
    echo '{ echo ${PATH} | grep ${GOROOT}/bin; } &>/dev/null || export PATH="${PATH}:${GOROOT}/bin"' >> /etc/profile
    echo '{ echo ${PATH} | grep ${GOPATH}/bin; } &>/dev/null || export PATH="${PATH}:${GOPATH}/bin"' >> /etc/profile

    # { echo ${PATH} | grep ${GOROOT}/bin; } &>/dev/null || export PATH="${PATH}:${GOROOT}/bin"
    # { echo ${PATH} | grep ${GOPATH}/bin; } &>/dev/null || export PATH="${PATH}:${GOPATH}/bin"

    export PATH="${PATH}:${GOROOT}/bin"
    export PATH="${PATH}:${GOPATH}/bin"

    if ! hash go 2>/dev/null; then
        error "install golang"
    fi
}

pkg(){
    command="${1}"

    run_pkg_manager "${command}" || { fix_pkg_manager; subtext "(retrying) ${package_manager} ${YELLOW}${command}"; ${package_manager} ${package_manager_parameters} ${command} &>/dev/null || error "${package_manager} ${command}"; }
}

run_pkg_manager(){
    task="${1}"

    subtext "${package_manager} ${YELLOW}${task}"

    ${package_manager} ${package_manager_parameters} ${task} &>/dev/null
}

fix_pkg_manager(){
    subtext "An error occured while trying to install packages. Trying to fix it..."

    case ${package_manager} in
    "yum")
        rm -f /var/lib/rpm/__* &>/dev/null
        # rm -f /var/lib/rpm/__db* &>/dev/null
        db_verify /var/lib/rpm/Packages &>/dev/null
        rpm --rebuilddb &>/dev/null

        run_pkg_manager "clean all"
    ;;
    *)
        rm -rf /var/lib/dpkg/updates/* &>/dev/null
        rm -rf /var/lib/apt/lists/* &>/dev/null
        rm /var/cache/apt/*.bin &>/dev/null

        run_pkg_manager "clean"
        run_pkg_manager "autoclean"
        run_pkg_manager "autoremove"
        run_pkg_manager "update --fix-missing"

        dpkg --clear-avail &>/dev/null
        yes | dpkg --configure -a &>/dev/null

        run_pkg_manager "-f install"
    ;;
    esac
}

configure_firewall(){
    header "Configuring firewall..."

    if [ -f "/etc/ssh/ssh_config" ]; then
        echo "$(grep -v 'port 22' /etc/ssh/ssh_config)" > /etc/ssh/ssh_config
    else
        touch /etc/ssh/ssh_config
    fi

    echo "port 22" >> /etc/ssh/ssh_config

    systemctl enable ssh &>/dev/null
    systemctl start ssh &>/dev/null

    iptables-save | awk '/^[*]/ { print $1 }
                     /^:[A-Z]+ [^-]/ { print $1 " ACCEPT" ; }
                     /COMMIT/ { print $0; }' | iptables-restore

    case ${os_name_short} in
    "CentOS_7")
        subheader "starting firewall..."

        systemctl disable ufw &>/dev/null
        systemctl stop ufw &>/dev/null

        systemctl daemon-reload &>/dev/null
        systemctl enable firewalld &>/dev/null
        systemctl start firewalld &>/dev/null

        subheader "setting ports/permissions..."

        default_zone="$(firewall-cmd --get-default-zone)"

        # delete rules
        firewall-cmd --zone=${default_zone} --permanent --remove-port=8895/tcp &>/dev/null
        firewall-cmd --zone=${default_zone} --permanent --remove-port=8895/udp &>/dev/null
        firewall-cmd --zone=${default_zone} --permanent --remove-port=8897/tcp &>/dev/null
        firewall-cmd --zone=${default_zone} --permanent --remove-port=8897/udp &>/dev/null
        firewall-cmd --zone=${default_zone} --permanent --remove-port=30666/tcp &>/dev/null
        firewall-cmd --zone=${default_zone} --permanent --remove-port=30666/udp &>/dev/null

        # add rules
        firewall-cmd --zone=${default_zone} --permanent --add-port=8895/tcp &>/dev/null
        firewall-cmd --zone=${default_zone} --permanent --add-port=8895/udp &>/dev/null
        firewall-cmd --zone=${default_zone} --permanent --add-port=8897/tcp &>/dev/null
        firewall-cmd --zone=${default_zone} --permanent --add-port=8897/udp &>/dev/null
        firewall-cmd --zone=${default_zone} --permanent --add-port=30666/tcp &>/dev/null
        firewall-cmd --zone=${default_zone} --permanent --add-port=30666/udp &>/dev/null
        firewall-cmd --zone=${default_zone} --permanent --add-service=ssh &>/dev/null

        firewall-cmd --reload &>/dev/null
    ;;
    *)
        subheader "starting firewall..."

        systemctl daemon-reload &>/dev/null
        systemctl enable ufw &>/dev/null
        systemctl start ufw &>/dev/null

        subheader "setting ports/permissions..."

        # delete rules
        ufw delete allow 8895 &>/dev/null
        ufw delete allow 8897 &>/dev/null
        ufw delete allow 30666 &>/dev/null

        # add rules
        ufw default allow outgoing &>/dev/null
        ufw default deny incoming &>/dev/null
        ufw allow ssh &>/dev/null
        ufw limit ssh &>/dev/null
        ufw insert 1 allow 8895 &>/dev/null
        ufw insert 1 allow 8897 &>/dev/null
        ufw insert 1 allow 30666 &>/dev/null
        ufw logging on &>/dev/null
        ufw --force enable &>/dev/null
    ;;
    esac
}

download_livenetwork_data(){
    header "Downloading necessary network files..."

    mkdir -p ${dir_live_net}/egem &>/dev/null

    subheader "config-qn.toml file"

    rm -rf ${dir_live_net}/egem/config-qn.toml &>/dev/null
    wget --no-check-certificate ${config_link} -O ${dir_live_net}/egem/config-qn.toml &>/dev/null || wget --no-check-certificate ${config_link} -O ${dir_live_net}/egem/config-qn.toml &>/dev/null || error "download config-qn.toml file"

    cd ${HOME}
}

install_go_egem(){
    header "Installing Go-EGEM..."

    # workaround for weird PATH issue
    export PATH="${PATH}:${GOROOT}/bin"
    export PATH="${PATH}:${GOPATH}/bin"


    # git pull is causing problems, better to install from scratch
    rm -rf ${dir_go_egem} &>/dev/null

    git clone --single-branch -b ${goegem_branch} ${egem_git} go-egem &>/dev/null || git clone --single-branch -b ${goegem_branch} ${egem_git} &>/dev/null || error "install Go-EGEM -> git clone"

    chmod -R 777 ${dir_go_egem} &>/dev/null
    cd ${dir_go_egem}

    make egem &>/dev/null || error "install Go-EGEM -> make egem"

    cd ${HOME}
}

install_nodestats(){
    cd ${dir_go_egem}

    # workaround for weird PATH issue
    export PATH="${PATH}:${GOROOT}/bin"
    export PATH="${PATH}:${GOPATH}/bin"

    make stats &>/dev/null || error "install NodeStats -> make stats"

    cd ${HOME}
}

create_services(){
    srv="${1}"

    header "Creating systemd services..."

    case ${srv} in
    "nodeupdater")
        create_nodeupdater_service
    ;;
    "egem")
        create_egem_service
    ;;
    "nodestats")
        create_nodestats_service
    ;;
    *)
        create_egem_service
        create_nodestats_service
        create_nodeupdater_service
    ;;
    esac

    # # just a precaution for local nodes
    # systemctl enable ssh &>/dev/null
    # systemctl start ssh &>/dev/null
}

create_nodeupdater_service(){
    subheader "Node Updater service"

    stop_services "${nodeupdater_service}"

    cd /etc/systemd/system/

    rm -rf ${nodeupdater_service} &>/dev/null
    touch ${nodeupdater_service} &>/dev/null

    echo "[Unit]" >> ${nodeupdater_service}
    echo "Description=Node Updater Service" >> ${nodeupdater_service}
    echo "After=network-online.target" >> ${nodeupdater_service}
    echo "" >> ${nodeupdater_service}
    echo "[Service]" >> ${nodeupdater_service}
    echo "User=root" >> ${nodeupdater_service}
    echo "Type=simple" >> ${nodeupdater_service}
    echo "TimeoutSec=15" >> ${nodeupdater_service}
    echo "Restart=always" >> ${nodeupdater_service}
    echo "RestartSec=5" >> ${nodeupdater_service}
    echo "" >> ${nodeupdater_service}
    echo "ExecStart=/usr/bin/${nodeupdater}" >> ${nodeupdater_service}
    echo "ExecStop=$(which pgrep) -f ${nodeupdater} | $(which xargs) $(which kill)" >> ${nodeupdater_service}
    echo "KillMode=process" >> ${nodeupdater_service}
    echo "" >> ${nodeupdater_service}
    echo "[Install]" >> ${nodeupdater_service}
    echo "WantedBy=multi-user.target" >> ${nodeupdater_service}
    echo "" >> ${nodeupdater_service}

    systemctl daemon-reload &>/dev/null
    systemctl enable ${nodeupdater_service} &>/dev/null

    cd ${HOME}
}

create_egem_service(){
    subheader "Go-EGEM service"

    stop_services "${egem_service}"

    cd /etc/systemd/system/

    if (("${total_ram}" > 1500)); then
        cache_size="1024"
    else
        cache_size=$((total_ram - 250))
    fi

    rm -rf ${egem_service} &>/dev/null
    touch ${egem_service} &>/dev/null

    echo "[Unit]" >> ${egem_service}
    echo "Description=Go-EGEM Service" >> ${egem_service}
    echo "After=network-online.target" >> ${egem_service}
    echo "" >> ${egem_service}
    echo "[Service]" >> ${egem_service}
    echo "User=root" >> ${egem_service}
    echo "Type=simple" >> ${egem_service}
    echo "TimeoutSec=15" >> ${egem_service}
    echo "Restart=always" >> ${egem_service}
    echo "RestartSec=5" >> ${egem_service}
    echo "" >> ${egem_service}

    case ${os_name_short} in
    "Raspbian_9"|"Raspbian_10")
        db_handles="1024"
    ;;
    *)
        db_handles="2048"
    ;;
    esac

    ln -sf ${dir_go_egem}/build/bin/egem /usr/bin/egem &>/dev/null

    ExecStart_line="ExecStart=/usr/bin/egem --config=${dir_live_net}/egem/config-qn.toml --datadir=${dir_live_net} --syncmode=fast --rpc --rpcapi=web3,eth,net --rpccorsdomain=* --rpcaddr=0.0.0.0 --rpcvhosts=*"

    if (("${cache_size}" > 100)); then
        ExecStart_line="${ExecStart_line} --cache=${cache_size}" >> ${egem_service}
    fi

    echo "${ExecStart_line}" >> ${egem_service}

    echo "ExecStop=$(which pgrep) -f /usr/bin/egem | $(which xargs) $(which kill)" >> ${egem_service}
    echo "KillMode=process" >> ${egem_service}
    echo "" >> ${egem_service}
    echo "[Install]" >> ${egem_service}
    echo "WantedBy=multi-user.target" >> ${egem_service}
    echo "" >> ${egem_service}

    systemctl daemon-reload &>/dev/null
    systemctl enable ${egem_service} &>/dev/null

    cd ${HOME}
}

create_nodestats_service() {
    subheader "NodeStats service"

    stop_services "${nodestats_service}"

    cd /etc/systemd/system/

    rm -rf ${nodestats_service} &>/dev/null
    touch ${nodestats_service} &>/dev/null

    echo "[Unit]" >> ${nodestats_service}
    echo "Description=NodeStats Service" >> ${nodestats_service}
    echo "After=egem.service" >> ${nodestats_service}
    echo "" >> ${nodestats_service}
    echo "[Service]" >> ${nodestats_service}
    echo "User=root" >> ${nodestats_service}
    echo "Type=simple" >> ${nodestats_service}
    echo "TimeoutSec=15" >> ${nodestats_service}
    echo "Restart=always" >> ${nodestats_service}
    echo "RestartSec=5" >> ${nodestats_service}
    echo "" >> ${nodestats_service}

    ln -sf ${dir_go_egem}/build/bin/stats /usr/bin/stats &>/dev/null

    echo "ExecStart=/usr/bin/stats" >> ${nodestats_service}
    echo "ExecStop=$(which pgrep) -f /usr/bin/stats | $(which xargs) $(which kill)" >> ${nodestats_service}
    echo "KillMode=process" >> ${nodestats_service}
    echo "" >> ${nodestats_service}
    echo "[Install]" >> ${nodestats_service}
    echo "WantedBy=multi-user.target" >> ${nodestats_service}
    echo "" >> ${nodestats_service}

    systemctl daemon-reload &>/dev/null
    systemctl enable ${nodestats_service} &>/dev/null

    cd ${HOME}
}

apply_ssd_tweaks(){
    header "Applying SSD related system tweaks..."

    # fstab tweaks
    # -------------------------------------------------------

    if [ ! -f /etc/fstab.backup ]; then
        cp /etc/fstab /etc/fstab.backup &>/dev/null
    fi

    old_line="$(grep ' / ' /etc/fstab | grep -v '^#')"
    old_mount_flags="$(echo ${old_line} | cut -d ' ' -f4)"

    if [ ! -z "${old_line}" ]; then
        if [ ! -z "${old_mount_flags}" ]; then

            new_mount_flags="${old_mount_flags}"

            # removed "discard" after reading that it may cause slowdowns
            for flag in "noatime" "nodiratime"
            do
                if ! $(echo ${new_mount_flags} | grep -q ${flag}); then
                    new_mount_flags="${flag},${new_mount_flags}"
                fi
            done

            new_line="$(echo ${old_line} | sed s*${old_mount_flags}*${new_mount_flags}*)"

            if [ ! -z "${new_line}" ]; then
                sed -i "s*${old_line}*${new_line}*" /etc/fstab
            fi
        fi
    fi

    # [:space:] is for horizontal and vertical spaces
    # [:blank:] is for horizontal spaces only
    echo "$(tr -s '[:blank:]' < /etc/fstab)" > /etc/fstab

    # swap usage tweaks
    # -------------------------------------------------------

    sed -i "/swappiness/d" /etc/sysctl.conf
    sed -i "/vfs_cache_pressure/d" /etc/sysctl.conf

    echo "vm.swappiness=10" >> /etc/sysctl.conf
    echo "vm.vfs_cache_pressure=50" >> /etc/sysctl.conf

    sysctl vm.swappiness=10 &>/dev/null
    sysctl vm.vfs_cache_pressure=50 &>/dev/null
}

show_next_steps(){
    clear

    node_ip=`wget -q --no-check-certificate -O - ${ipcheck_url}`

    sleep 0.25s

    echo ${CYAN}"-------------------------------------------------------------------"${NORMAL}
    echo ${GREEN}"EGEM Quarry Node setup has finished succesfully!"${NORMAL}
    echo ${YELLOW}"Please follow these steps to activate your node payments:"${NORMAL}
    echo
    echo ${MAGENTA}"  1) Make sure you have the necessary balance in your wallet"${NORMAL}
    echo ${CYAN}"      If this is a 10k (Tier 1) node  ->  at least 10k EGEM"${NORMAL}
    echo ${CYAN}"      If this is a 30k (Tier 2) node  ->  at least 40k EGEM"${NORMAL}
    echo
    echo ${MAGENTA}"  2) Direct message 'The EGEM Master' bot these commands"${NORMAL}
    echo ${CYAN}"      If this is a 10k (Tier 1) node:"${NORMAL}
    echo ${YELLOW}"          /register"${NORMAL}
    echo ${YELLOW}"          /setaddress YourWalletAddress"${NORMAL}
    echo ${YELLOW}"          /an 1 1 ${node_ip}"${NORMAL}
    echo
    echo ${CYAN}"      If this is a 30k (Tier 2) node:"${NORMAL}
    echo ${YELLOW}"          /an 2 1 ${node_ip}"${NORMAL}
    echo
    echo ${CYAN}"Check ${YELLOW}wiki.egem.io/qnregister${CYAN} for 3 or more nodes on same account."${NORMAL}
    echo
    echo ${GREEN}"That's all. Press Enter to see the summary please."${NORMAL}
    echo ${CYAN}"-------------------------------------------------------------------"${NORMAL}
    read key

    clear

    echo ${CYAN}"-------------------------------------------------------------------"${NORMAL}
    echo ${YELLOW}"Waiting a few seconds to let the software start..."${NORMAL}
    echo ${CYAN}"-------------------------------------------------------------------"${NORMAL}

    sleep 3s

    summary
}

summary(){
    clear

    node_ip=`wget -q --no-check-certificate -O - ${ipcheck_url}`
    installed_golang_version=`go version | awk '{print $3}' | cut -c 3-`
    installed_goegem_version="$(/usr/bin/egem version | grep -i '^version: ' | awk '{print $2}' | cut -d '-' -f1 )"

    if [ -z "$(grep -i execstart /etc/systemd/system/${egem_service} | grep -i full | grep -i archive)" ]; then
        archive_mode="disabled"
    else
        archive_mode="enabled"
    fi

    clear

    echo ${BLUE}"-------------------------------------------------------------------"${NORMAL}
    echo ${GREEN}" EGEM Quarry Node Status"${MAGENTA}"  |  "${CYAN}"for more options:  ${YELLOW}nodemanager --help" ${NORMAL}
    echo ${BLUE}"-------------------------------------------------------------------"${NORMAL}

    echo "${MAGENTA}      Script version           :  ${WHITE}${script_version}"
    echo "${MAGENTA}      Go-lang version          :  ${WHITE}${installed_golang_version:-unknown}"
    echo "${MAGENTA}      Go-EGEM version          :  ${WHITE}${installed_goegem_version:-unknown}"
    echo
    echo "${MAGENTA}      Archive Mode             :  ${YELLOW}${archive_mode}"
    echo
    echo "${MAGENTA}      Operating System         :  ${CYAN}"${os_name_long}${NORMAL}
    echo "${MAGENTA}      Public IP Address        :  ${CYAN}${node_ip}"${NORMAL}
    echo

    if [ ! -z "$(pgrep -f /usr/bin/stats | grep -v $$)" ]; then
        echo "${MAGENTA}      NodeStats Status         :  ${GREEN}working"${NORMAL}
    else
        echo "${MAGENTA}      NodeStats Status         :  ${RED}not working"${NORMAL}
    fi

    if [ ! -z "$(pgrep -f /usr/bin/egem | grep -v $$)" ]; then
        echo "${MAGENTA}      Go-EGEM Status           :  ${GREEN}working"${NORMAL}
    else
        echo "${MAGENTA}      Go-EGEM Status           :  ${RED}not working"${NORMAL}
    fi

    echo ${BLUE}"-------------------------------------------------------------------"${NORMAL}
    echo

    sleep 0.25s
}

show_warning(){
    clear
    echo ${CYAN}"-------------------------------------------------------------------"${NORMAL}
    echo ${MAGENTA}"You are about to install EGEM Quarry Node from scratch."${NORMAL}
    echo
    echo ${YELLOW}"This will delete/overwrite previous Quarry Node files/folders."${NORMAL}
    echo
    echo ${MAGENTA}"Press ${GREEN}Enter${MAGENTA} to continue or press ${RED}Ctrl+C${MAGENTA} to terminate the script."${NORMAL}
    echo ${CYAN}"-------------------------------------------------------------------"${NORMAL}
    read key
    clear
}

show_log(){
    clear
    echo ${CYAN}"-------------------------------------------------------------------"${NORMAL}
    echo ${GREEN}"EGEM Log"${NORMAL}
    echo ${CYAN}"-------------------------------------------------------------------"${NORMAL}

    tail -n 15 ${egemlog}

    echo ${CYAN}"-------------------------------------------------------------------"${NORMAL}
}

extrasteps(){
    whatsteps="${1}"

    cd ${HOME}

    case ${whatsteps} in
    "before")
        bash ${dir_scripts}/${presetup}
    ;;
    "after")
        nohup ${dir_scripts}/${postsetup} &>/dev/null &
    ;;
    esac
}

early_header(){
    text="${1}"

    echo ${GREEN}"-------------------------------------------------------------------"
    echo "${WHITE}[${GREEN}*${WHITE}] ${CYAN}${text}${NORMAL}"
}

header(){
    text="${1}"

    echo ${GREEN}"-------------------------------------------------------------------"
    echo "${WHITE}[${GREEN}${step}${WHITE}] ${CYAN}${text}${NORMAL}"

    ((step++))
}

subheader(){
    text="${1}"

    echo "    ${WHITE}> ${MAGENTA}${text}${NORMAL}"
}

subtext(){
    text="${1}"

    echo "      ${GREEN}- ${CYAN}${text}${NORMAL}"
}

error(){
    failed_step="${1}"

    echo ${RED}"Error! Install failed at this step ->  ${YELLOW}${failed_step}"${NORMAL}
    echo ${RED}"Please re-run the script. Contact developers if it happens again."${NORMAL}
    exit 1
}

help_text(){
    echo ${BLUE}"-------------------------------------------------------------------"${NORMAL}
    echo ${GREEN}" EGEM Quarry Node Manager Parameters"${NORMAL}
    echo ${BLUE}"-------------------------------------------------------------------"${NORMAL}
    echo ${YELLOW}"    --help          ${GREEN}-  ${MAGENTA}displays this help text"${NORMAL}
    echo ${YELLOW}"    --status        ${GREEN}-  ${MAGENTA}shows Quarry Node status"${NORMAL}
    echo ${YELLOW}"    --show-log      ${GREEN}-  ${MAGENTA}shows last part of EGEM log file"${NORMAL}
    echo
    echo ${YELLOW}"    --start         ${GREEN}-  ${MAGENTA}starts Quarry Node"${NORMAL}
    echo ${YELLOW}"    --restart       ${GREEN}-  ${MAGENTA}restarts Quarry Node"${NORMAL}
    echo ${YELLOW}"    --stop          ${GREEN}-  ${MAGENTA}stops Quarry Node"${NORMAL}
    echo
    echo ${YELLOW}"    --install-all   ${GREEN}-  ${MAGENTA}installs everything from scratch (default)"${NORMAL}
    echo ${YELLOW}"    --self-update   ${GREEN}-  ${MAGENTA}updates script"${NORMAL}
    echo ${YELLOW}"    --update-egem   ${GREEN}-  ${MAGENTA}updates Go-EGEM"${NORMAL}
    echo ${YELLOW}"    --update-stats  ${GREEN}-  ${MAGENTA}updates NodeStats"${NORMAL}
    echo
    # echo ${YELLOW}"    --reset-chain   ${GREEN}-  ${MAGENTA}deletes blockchain data"${NORMAL}
    echo ${YELLOW}"    --fix           ${GREEN}-  ${MAGENTA}tries to fix configuration problems"${NORMAL}
    echo ${BLUE}"-------------------------------------------------------------------"${NORMAL}
    echo

    exit 0
}

script_version="0.6.8"

# -----------------------------------------------------------------
# color codes for echo commands
NORMAL=$'\e[0m'
RED=$'\e[31;01m'
GREEN=$'\e[32;01m'
YELLOW=$'\e[33;01m'
BLUE=$'\e[34;01m'
MAGENTA=$'\e[35;01m'
CYAN=$'\e[36;01m'
WHITE=$'\e[97;01m'
# -----------------------------------------------------------------

prm="${1}"

killthis ${nodemanager} "apt-get" &>/dev/null

case ${prm} in
"-v" | "v" | "--version" | "version")
    echo
    echo ${MAGENTA}"EGEM Node Manager"${NORMAL}
    echo ${CYAN}"version: ${GREEN}${script_version}"${NORMAL}
    echo

    exit 0
;;
"--help")
    help_text
;;
esac

if [ $EUID -ne 0 ]; then
    echo
    echo ${RED}"Error: This script must be run as root. Terminating..."${NORMAL}
    echo
    exit 1
fi

cd ${HOME}

clear

shopt -s extglob
mesg y < $(tty)

step="1"

case ${prm} in
""|"--install-all")
    show_warning
;;
esac

if hash apt-get &>/dev/null; then
    package_manager="apt-get"
    package_manager_parameters='--quiet -y'

    # variable to disable graphical prompt for apt-get updates
    export DEBIAN_FRONTEND=noninteractive
elif hash yum &>/dev/null; then
    package_manager="yum"
    package_manager_parameters='-q -y -t'
else
    package_manager=""
    package_manager_parameters=""

    echo ${RED}"Error: Current package manager is not supported. Terminating..."${NORMAL}
    echo

    exit 1
fi

# -------------------------------------------------------

case ${prm} in
"--pull-updates"|"--self-update"|"--update-stats"|"--update-egem"|"--fix"|""|"--install-all")
    # check package dependencies
    check_dependencies
;;
esac

# -------------------------------------------------------

set_env
collect_info

# -------------------------------------------------------

case ${os_name_short} in
"unidentified")
    echo ${RED}"Error: Current operating system is not supported."${NORMAL}
    echo ${YELLOW}"Please use one of these: ${GREEN}Debian 8/9/10, Raspbian 9/10, Ubuntu 16/18, CentOS 7"${NORMAL}
    echo
    echo ${RED}"Terminating..."${NORMAL}
    echo

    exit 1
;;
esac

if (("${total_ram}" < "${min_ram}")); then
    echo ${RED}"Error: There is not enough ram to run a Quarry Node."
    echo ${YELLOW}"Minimum reqiured RAM: ${CYAN}${min_ram}MB"${NORMAL}
    echo ${YELLOW}"Current system RAM: ${RED}${total_ram}MB"${NORMAL}
    echo
    exit 1
fi

case ${prm} in
""|"--install-all")
    stop_services "${egem_service} ${nodestats_service}"
;;
"--update-egem"|"--fix")
    stop_services "${egem_service} ${nodestats_service}"
;;
"--update-stats")
    stop_services "${nodestats_service}"
;;
# "--reset-chain")
#     stop_services "${egem_service}"
# ;;
esac

case ${prm} in
"--start")
    start_services
;;
"--restart")
    restart_services
;;
"--stop")
    stop_services
;;
"--pull-updates")
    # download setup scripts
    set_env "force"
;;
"--self-update")
    # download setup scripts
    set_env "force"
    extrasteps "before"
    configure_firewall
    download_livenetwork_data
    create_services
    extrasteps "after"
    restart_services "${nodeupdater_service}"
;;
"--update-egem")
    configure_firewall
    download_livenetwork_data
    install_golang
    install_go_egem
    install_nodestats
    create_services "egem"
    create_services "nodestats"
    start_services
;;
"--update-stats")
    configure_firewall
    install_nodestats
    create_services "nodestats"
    start_services
;;
"--status")
    summary
;;
"--fix")
    # download setup scripts
    set_env "force"
    extrasteps "before"
    install_necessary_packages
    configure_firewall
    download_livenetwork_data
    install_golang
    install_go_egem
    install_nodestats
    create_services
    extrasteps "after"
    restart_services
;;
"--show-log")
    show_log
;;
""|"--install-all")
    # download setup scripts
    set_env "force"
    extrasteps "before"
    create_swapfile
    install_necessary_packages
    configure_firewall
    download_livenetwork_data
    install_golang
    install_go_egem
    install_nodestats
    create_services
    apply_ssd_tweaks
    extrasteps "after"
    restart_services
    show_next_steps
;;
*)
    help_text
;;
esac

exit 0 &>/dev/null
